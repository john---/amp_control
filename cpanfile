requires 'Net::Async::MPD';
requires 'IO::Async::Timer::Countdown';
requires 'IO::Async::Loop';

requires 'RPi::Pin';
requires 'RPi::Const';
